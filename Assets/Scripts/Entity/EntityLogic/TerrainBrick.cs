﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityGameFramework.Runtime;

namespace CatPaw
{
    /// <summary>
    /// 地砖逻辑
    /// </summary>
    public class TerrainBrick : Entity
    {
        [SerializeField]
        private TerrainBrickData m_TerrainBrickData = null;

#if UNITY_2017_3_OR_NEWER
        protected override void OnInit(object userData)
#else
        protected internal override void OnInit(object userData)
#endif
        {
            base.OnInit(userData);
        }

#if UNITY_2017_3_OR_NEWER
        protected override void OnShow(object userData)
#else
        protected internal override void OnShow(object userData)
#endif
        {
            base.OnShow(userData);

            m_TerrainBrickData = userData as TerrainBrickData;
            if (m_TerrainBrickData == null)
            {
                Log.Error("Asteroid data is invalid.");
                return;
            }

            //m_RotateSphere = Random.insideUnitSphere;
        }

#if UNITY_2017_3_OR_NEWER
        protected override void OnUpdate(float elapseSeconds, float realElapseSeconds)
#else
        protected internal override void OnUpdate(float elapseSeconds, float realElapseSeconds)
#endif
        {
            base.OnUpdate(elapseSeconds, realElapseSeconds);

            //CachedTransform.Translate(Vector3.back * elapseSeconds, Space.World);
            //CachedTransform.Rotate(m_RotateSphere * m_TerrainBrickData.AngularSpeed * elapseSeconds, Space.Self);
        }

    }
}
