﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatPaw
{
    /// <summary>
    /// 地砖数据
    /// </summary>
    public class TerrainBrickData : EntityData
    {
        public TerrainBrickData(int entityId, int typeId) : base(entityId, typeId)
        {
        }
    }
}
