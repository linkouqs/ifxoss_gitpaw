﻿using GameFramework.DataTable;
using System;

namespace CatPaw
{
    public class DRTerrain : IDataRow
    {
        /// <summary>
        /// 地图数据
        /// </summary>
        public string[] Data { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public int Id
        {
            get;
            protected set;
        }

        public void ParseDataRow(string dataRowText)
        {

        }
    }
}
