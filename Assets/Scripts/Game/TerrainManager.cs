﻿using GameFramework;
using GameFramework.Resource;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityGameFramework.Runtime;

namespace CatPaw
{
    public class TerrainManager
    {
        #region 加载地形配置

        LoadAssetCallbacks m_LoadTerrainDataTableCallbacks;
        List<string> m_ToLoadTerrainDataTables = new List<string>();
        Dictionary<string, TextAsset> m_TerrainDataTables = new Dictionary<string, TextAsset>();

        void LoadTerrainDataTableSuccessCallBack(string assetName, object asset, float duration, object userData)
        {
            m_TerrainDataTables.Add(assetName, (TextAsset)asset);

            if (m_ToLoadTerrainDataTables.Contains(assetName))
            {
                m_ToLoadTerrainDataTables.Remove(assetName);
            }

            if (m_ToLoadTerrainDataTables.Count == 0)
            {
                // 加载地形配置结束

                LoadTerrainUnitPrefabs();
            }
        }

        void LoadTerrainDataTableFailureCallBack(string assetName, LoadResourceStatus status, string errorMessage, object userData)
        {
            Log.Error(errorMessage);
        }

        void LoadTerrainDataTableUpdateCallBack(string assetName, float progress, object userData)
        {

        }

        void LoadTerrainDataTableDependencyAssetCallBack(string assetName, string dependencyAssetName,
        int loadedCount, int totalCount, object userData)
        {

        }

        public void LoadTerrainDataTables(string terrainName)
        {
            if (m_LoadTerrainDataTableCallbacks != null)
            {
                throw new GameFrameworkException("Please invoke UnloadTerrainDataTables() at first.");
            }

            // 设置回调

            m_LoadTerrainDataTableCallbacks = new LoadAssetCallbacks(
                LoadTerrainDataTableSuccessCallBack,
                LoadTerrainDataTableFailureCallBack,
                LoadTerrainDataTableUpdateCallBack,
                LoadTerrainDataTableDependencyAssetCallBack
            );

            // 记录要加载的地形配置

            m_ToLoadTerrainDataTables.Add(AssetUtility.GetTerrainDataAsset(string.Format("{0}/{0}_0", terrainName)));
            m_ToLoadTerrainDataTables.Add(AssetUtility.GetTerrainDataAsset(string.Format("{0}/{0}_1", terrainName)));

            // 加载地形配置

            var arr = m_ToLoadTerrainDataTables.ToArray();

            foreach (var assetName in arr)
            {
                GameEntry.Resource.LoadAsset(assetName, m_LoadTerrainDataTableCallbacks);
            }

        }

        public void UnloadTerrainDataTables()
        {
            DeleteTerrain();

            UnloadTerrainUnitPrefabs();

            foreach (var kvp in m_TerrainDataTables)
            {
                GameEntry.Resource.UnloadAsset(kvp.Value);
            }

            m_TerrainDataTables.Clear();

            m_LoadTerrainDataTableCallbacks = null;
        }

        #endregion

        #region 加载地形预制

        LoadAssetCallbacks m_LoadTerrainUnitPrefabCallbacks;
        List<string> m_ToLoadTerrainUnitPrefabs = new List<string>();
        Dictionary<string, GameObject> m_TerrainUnitPrefabs = new Dictionary<string, GameObject>();

        void LoadTerrainUnitPrefabSuccessCallBack(string assetName, object asset, float duration, object userData)
        {
            m_TerrainUnitPrefabs.Add(assetName, (GameObject)asset);

            if (m_ToLoadTerrainUnitPrefabs.Contains(assetName))
            {
                m_ToLoadTerrainUnitPrefabs.Remove(assetName);
            }

            if (m_ToLoadTerrainUnitPrefabs.Count == 0)
            {
                // 加载地形预制结束

                CreateTerrain();
            }
        }

        void LoadTerrainUnitPrefabFailureCallBack(string assetName, LoadResourceStatus status, string errorMessage, object userData)
        {
            Log.Error(errorMessage);
        }

        void LoadTerrainUnitPrefabUpdateCallBack(string assetName, float progress, object userData)
        {

        }

        void LoadTerrainUnitPrefabDependencyAssetCallBack(string assetName, string dependencyAssetName,
        int loadedCount, int totalCount, object userData)
        {

        }

        void LoadTerrainUnitPrefabs()
        {
            if (m_LoadTerrainUnitPrefabCallbacks != null)
            {
                throw new GameFrameworkException("Please invoke UnloadTerrainUnitPrefabs() at first.");
            }

            // 设置回调

            m_LoadTerrainUnitPrefabCallbacks = new LoadAssetCallbacks(
            LoadTerrainUnitPrefabSuccessCallBack,
            LoadTerrainUnitPrefabFailureCallBack,
            LoadTerrainUnitPrefabUpdateCallBack,
            LoadTerrainUnitPrefabDependencyAssetCallBack
                );

            // 记录要加载的地形预制

            foreach (var kvp in m_TerrainDataTables)
            {
                string[] mapRows = kvp.Value.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string mapRow in mapRows)
                {
                    string[] mapUnits = mapRow.Split(',');
                    foreach (string mapUnit in mapUnits)
                    {
                        var assetName = AssetUtility.GetTerrainAsset(mapUnit);

                        if (m_ToLoadTerrainUnitPrefabs.Contains(assetName))
                        {
                            continue;
                        }

                        m_ToLoadTerrainUnitPrefabs.Add(assetName);
                    }
                }
            }

            // 加载地形预制

            var arr = m_ToLoadTerrainUnitPrefabs.ToArray();

            foreach (var assetName in arr)
            {
                GameEntry.Resource.LoadAsset(assetName, m_LoadTerrainUnitPrefabCallbacks);
            }

        }

        void UnloadTerrainUnitPrefabs()
        {
            foreach (var kvp in m_TerrainUnitPrefabs)
            {
                GameEntry.Resource.UnloadAsset(kvp.Value);
            }

            m_TerrainUnitPrefabs.Clear();

            m_LoadTerrainUnitPrefabCallbacks = null;
        }

        #endregion

        #region 创建地形

        Transform m_Terrain;

        void CreateTerrain()
        {
            if (m_Terrain != null)
            {
                throw new GameFrameworkException("Please invoke DeleteTerrain() at first.");
            }

            m_Terrain = new GameObject("Terrain").transform;

            int layer = 0;
            float offset = 1f;

            foreach (var kvp in m_TerrainDataTables)
            {
                int x = 0;

                string[] mapRows = kvp.Value.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string mapRow in mapRows)
                {
                    int z = 0;

                    string[] mapUnits = mapRow.Split(',');
                    foreach (string mapUnit in mapUnits)
                    {
                        var assetName = AssetUtility.GetTerrainAsset(mapUnit);

                        var prefab = m_TerrainUnitPrefabs[assetName];

                        var unit = GameObject.Instantiate(prefab, m_Terrain);

                        unit.transform.localPosition = new Vector3(x * offset, layer, z * offset);

                        z++;
                    }

                    x++;
                }

                layer++;
            }

            // 创建地形结束

            // TODO.


            Log.Warning("创建地形结束.");
        }

        void DeleteTerrain()
        {
            if (m_Terrain == null)
                return;

            GameObject.Destroy(m_Terrain);
            m_Terrain = null;
        }

        #endregion
    }
}
