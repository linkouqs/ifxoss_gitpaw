﻿using System;
using GameFramework;
using GameFramework.DataTable;
using GameFramework.Event;
using GameFramework.Resource;
using UnityEditor;
using UnityEngine;
using UnityGameFramework.Runtime;

using System.Collections;
using System.Collections.Generic;

namespace CatPaw
{
    public class SurvivalGame : GameBase
    {
        private LoadAssetCallbacks m_LoadGameAssetCatCallbacks;
        private float m_ElapseSeconds = 0f;

        /// <summary>
        /// 地图名称
        /// </summary>
        private string m_terrainName = "a1";
        /// <summary>
        /// 下层地图
        /// </summary>
        private string TerrainTextAssetsDown;
        /// <summary>
        /// 上层地图
        /// </summary>
        private string TerrainTextAssetsUp;


        private bool _isParsedTerrainDown = false;
        private bool _isParsedTerrainUp = false;
        private bool _isLoadedFloorBrickDown = false;
        private bool _isLoadedFloorBrickUp = false;
        private bool _isLoadedPlayerCat = false;
        private bool _isCreatedTerrain = false;
        private bool _isLoadingPlayer = false;



        public DateTime BeginTime;

        public TerrainManager terrainManager;

        public SurvivalGame(string TerrainName)
        {
            m_terrainName = TerrainName;

            terrainManager = new TerrainManager();
        }

        public override void Initialize()
        {
            base.Initialize();

            //// GF 加载场景资源
            //// 加载地图
            //m_LoadGameAssetCatCallbacks = new LoadAssetCallbacks(LoadSceneAssetsSuccessCallBack, 
            //LoadSceneAssetsFailureCallBack, LoadSceneAssetsUpdateCallBack, LoadSceneAssetsDependencyAssetCallBack);

            //this.TerrainTextAssetsDown = string.Format("{0}/{0}_0", m_terrainName);
            //this.TerrainTextAssetsUp = string.Format("{0}/{0}_1", m_terrainName);
            //GameEntry.Resource.LoadAsset(AssetUtility.GetTerrainDataAsset(TerrainTextAssetsDown), m_LoadGameAssetCatCallbacks);
            //GameEntry.Resource.LoadAsset(AssetUtility.GetTerrainDataAsset(TerrainTextAssetsUp), m_LoadGameAssetCatCallbacks);

            // 加载地形配置->加载地形预制->创建地形

            terrainManager.LoadTerrainDataTables(this.m_terrainName);
        }


        /// <summary>
        /// 游戏开始
        /// </summary>
        public void OnGameStart()
        {
            this.BeginTime = DateTime.Now;
        }

        public override GameMode GameMode
        {
            get
            {
                return GameMode.Survival;
            }
        }

        public override void Update(float elapseSeconds, float realElapseSeconds)
        {
            base.Update(elapseSeconds, realElapseSeconds);



            m_ElapseSeconds += elapseSeconds;
            if (m_ElapseSeconds >= 1f)
            {
                m_ElapseSeconds = 0f;

                GameEntry.Entity.ShowBrick(new TerrainBrickData(GameEntry.Entity.GenerateSerialId(), 0)
                {
                    Position = new Vector3(12, -2f, -36),
                });
            }
        }

    }
}
